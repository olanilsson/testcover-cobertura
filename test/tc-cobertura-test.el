;;; tc-cobertura-test.el --- Tests for testcover-cobertura

;; Copyright (C) 2014  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;;; Commentary:

;; ERT tests for testcover-cobertura.el

;;; Code:

(require 'ert)
(require 'testcover-cobertura)

;; Utilitity functions:

(defmacro tc-cobertura-inhibit-message (&rest body)
  "Inhibit messages for BODY."
  (declare (debug t))
  `(flet ((message (&rest _)))
     ,@body))

(defun tc-cobertura-testcover-form (string)
  "Read a form from STRING and return it instrumented for testcover."
  (tc-cobertura-inhibit-message
   (with-temp-buffer
	 (insert string "\n")
	 (goto-char (point-min))
	 (let* ((edebug-all-defs t)
			(edebug-all-forms t)
			(x (edebug-read-and-maybe-wrap-form)))
	   (testcover-reinstrument x)
	   x))))

(defmacro tc-with-test-file (file &rest body)
  "Load FILE and execute BODY while FILE has `testcover' activated.
`testcover-start' is called before BODY and `testcover-end' after."
  (declare (debug t) (indent 1))
  `(progn (tc-cobertura-inhibit-message
	 (load-file ,file)
	 (testcover-start ,file))
	,@body
	(testcover-end ,file)))

;; tests declared in test1.el, input to the ert tests in this file
;; Adding argument lists to declare-function will give errors for some reason.
(declare-function tc-test-test-1 "test1")
(declare-function tc-test-test-2 "test1")
(declare-function tc-test-test-3 "test1")
(declare-function tc-test-1value "test1")
(declare-function tc-test-lambda "test1")

(ert-deftest tc-cobertura-test-used-indexes-1 ()
  "Negative testcase for `testcover-cobertura--used-indexes'.
`testcover-cobertura--used-indexes' should return nil when passed
forms that contain no `testcover-after' function calls."
  :tags '(testcover-cobertura--used-indexes)
  (should-not (testcover-cobertura--used-indexes '(a)))
  (should-not (testcover-cobertura--used-indexes '(a b c d)))
  (should-not (testcover-cobertura--used-indexes '((a b) (1 2)))))

(ert-deftest tc-cobertura-test-used-indexes-2 ()
  "Test `testcover-cobertura--used-indexes' against a few small functions."
  :tags '(testcover-cobertura--used-indexes)
  (ert-info ("(lambda (a) (if a 1 2))")
	(let* ((tcform (tc-cobertura-testcover-form "(lambda (a) (if a 1 2))"))
		   (indexes (testcover-cobertura--used-indexes tcform)))
	  (should (= 2 (length indexes)))
	  (should (= (nth 0 indexes) 1))
	  (should (= (nth 1 indexes) 2))))
  (ert-info ("(lambda (a b c) (if a b c))")
	(let* ((tcform (tc-cobertura-testcover-form "(lambda (a b c) (if a b c))"))
		   (indexes (testcover-cobertura--used-indexes tcform)))
	  (should (= 4 (length indexes)))
	  (should (= (nth 0 indexes) 1))
	  (should (= (nth 1 indexes) 2))
	  (should (= (nth 2 indexes) 3))
	  (should (= (nth 3 indexes) 4))))
  (ert-info ("(lambda (a b c) (if a (if b c) (if c b)))")
	(let* ((tcform (tc-cobertura-testcover-form "(lambda (a b c) (if a (if b c) (if c b)))"))
		   (indexes (testcover-cobertura--used-indexes tcform))
		   (expected '(1 3 4 5 7 8 9 10)))
	  (should (= (length indexes) (length expected)))
	  (dotimes (i (length expected))
		(ert-info ((format "checking indexes[%d] == expected[%d]" i i))
		  (should (= (nth i indexes) (nth i expected))))))))



(defvar tc-this-file (or load-file-name (buffer-file-name)))
(assert (string= (file-name-nondirectory tc-this-file) "tc-cobertura-test.el"))
(defvar tc-test1 (expand-file-name "test1.el" (file-name-directory tc-this-file)))

(defmacro tc-defun-report (fut expected &rest body)
  "Simplifies writing testcases for `testcover-cobertura-defun-report'.
FUT is the name of the function used as test input.  EXPECTED is
the expected return value.  BODY is executed after FUT has been
instrumented but before `testcover-cobertura-defun-report' is
called."
  `((tc-with-test-file tc-test1
	(progn ,@body)
	(should (equal expected (testcover-cobertura-defun-report (quote fut)))))))

(ert-deftest tc-cobertura-test-defun-report-1 ()
  "Verify the results of `testcover-cobertura-defun-report' on `tc-test-test-1'.
In this testcase `tc-test-test-1' is instrumented but never
called so coverage should be 0."
  :expected-result :failed ;; no testpoint, we cannot distinguish between executed or not.
  :tags '(testcover-cobertura-defun-report)
  (tc-with-test-file tc-test1
	(let ((expected '(method :name "tc-test-test-1" :signature "(tc-test-test-1)"
							 :line-rate 0.0 :branch-rate 0.0 (lines)))
		  (report (testcover-cobertura-defun-report 'tc-test-test-1)))
	  (should (equal expected report)))))

(ert-deftest tc-cobertura-test-defun-report-2 ()
  "Verify the results of `testcover-cobertura-defun-report' on `tc-test-test-1'.
In this testcase `tc-test-test-1' is instrumented and called."
  :expected-result :passed ;; no testpoint, we cannot distinguish between executed or not.
  :tags '(testcover-cobertura-defun-report)
  (tc-with-test-file tc-test1
	(tc-test-test-1)
	(let ((expected '(method :name "tc-test-test-1" :signature "(tc-test-test-1)"
							 :line-rate 1.0 :branch-rate 1.0 (lines)))
		  (report (testcover-cobertura-defun-report 'tc-test-test-1)))
	  (should (equal expected report)))))

(ert-deftest tc-cobertura-test-defun-report-3 ()
  "Verify the results of `testcover-cobertura-defun-report' on `tc-test-test-2'.
In this testcase `tc-test-test-2' is instrumented but never
called so coverage is 0."
  :tags '(testcover-cobertura-defun-report)
  (tc-with-test-file tc-test1
	(let ((expected '(method :name
							 "tc-test-test-2"
							 :signature "(tc-test-test-2 A)" :line-rate 0.0 :branch-rate 0.0
							 (lines
							  (line :number 352 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 357 :hits 0 :branch "true" :condition-coverage 0.0))))
		  (report (testcover-cobertura-defun-report 'tc-test-test-2)))
	  (should (equal expected report)))))

(ert-deftest tc-cobertura-test-defun-report-4 ()
  "Verify the results of `testcover-cobertura-defun-report' on `tc-test-test-2'.
In this testcase `tc-test-test-2' is instrumented and called."
  :tags '(testcover-cobertura-defun-report)
  (tc-with-test-file tc-test1
	(tc-test-test-2 1)
	(tc-test-test-2 0)
	(let ((expected '(method :name "tc-test-test-2"
							 :signature "(tc-test-test-2 A)" :line-rate 1.0 :branch-rate 1.0
							 (lines
							  (line :number 352 :hits 2 :branch "true" :condition-coverage 1.0)
							  (line :number 357 :hits 2 :branch "true" :condition-coverage 1.0))))
		  (report (testcover-cobertura-defun-report 'tc-test-test-2)))
	  (should (equal expected report)))))

(ert-deftest tc-cobertura-test-defun-report-5 ()
  "Verify the results of `testcover-cobertura-defun-report' on `tc-test-test-3'.
In this testcase `tc-test-test-3' is instrumented but never
called so coverage is 0."
  :tags '(testcover-cobertura-defun-report)
  (tc-with-test-file tc-test1
	(let ((expected '(method :name "tc-test-test-3"
							 :signature "(tc-test-test-3 A B C)" :line-rate 0.0 :branch-rate 0.0
							 (lines
							  (line :number 403 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 409 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 411 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 413 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 414 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 420 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 422 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 424 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 425 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 426 :hits 0 :branch "true" :condition-coverage 0.0)
							  )))
		  (report (testcover-cobertura-defun-report 'tc-test-test-3)))
	  (should (equal expected report)))))

(ert-deftest tc-cobertura-test-defun-report-6 ()
  "Verify the results of `testcover-cobertura-defun-report' on `tc-test-test-3'.
In this testcase `tc-test-test-3' is instrumented and called."
  :tags '(testcover-cobertura-defun-report)
  (tc-with-test-file tc-test1
	(tc-test-test-3 1 2 3)
	(let ((expected '(method :name "tc-test-test-3"
							 :signature "(tc-test-test-3 A B C)" :line-rate 0.5 :branch-rate 0.25
							 (lines
							  (line :number 403 :hits 1 :branch "true" :condition-coverage 0.5)
							  (line :number 409 :hits 1 :branch "true" :condition-coverage 0.5)
							  (line :number 411 :hits 1 :branch "true" :condition-coverage 0.5)
							  (line :number 413 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 414 :hits 1 :branch "true" :condition-coverage 0.5)
							  (line :number 420 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 422 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 424 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 425 :hits 0 :branch "true" :condition-coverage 0.0)
							  (line :number 426 :hits 1 :branch "true" :condition-coverage 0.5)
							  )))
		  (report (testcover-cobertura-defun-report 'tc-test-test-3)))
	  (should (equal expected report)))))

(ert-deftest tc-cobertura-test-defun-report-7 ()
  "Verify that `testcover-coberture-defun-report' throws an error
for uninstrumented functions."
  :tags '(testcover-cobertura-defun-report)
  (load-file tc-test1)
  (should-error (testcover-cobertura-defun-report 'tc-test-test-1))
  (should-error (testcover-cobertura-defun-report 'tc-test-test-2))
  (should-error (testcover-cobertura-defun-report 'tc-test-test-3)))


(ert-deftest tc-cobertura-test-defun-report-8 ()
  "Verify that `testcover-cobertura-defun-report' handles
`1value' sexps as expected.  `1value' sexps should return \":hits 2\"
and \":branch false\"."
  :tags '(testcover-cobertura-defun-report)
  (tc-with-test-file tc-test1
	(tc-test-1value 1)
	(let ((expected '(method :name "tc-test-1value"
							 :signature "(tc-test-1value A)" :line-rate 1.0 :branch-rate 0.7
							 (lines
							  (line :number 473 :hits 2 :branch "true" :condition-coverage 1.0)
							  (line :number 474 :hits 2 :branch "true" :condition-coverage 1.0)
							  (line :number 488 :hits 1 :branch "true" :condition-coverage 0.5)
							  (line :number 489 :hits 1 :branch "true" :condition-coverage 0.5)
							  (line :number 490 :hits 1 :branch "true" :condition-coverage 0.5)
							  (line :number 491 :hits 2 :branch "false"))))
		  (report (testcover-cobertura-defun-report 'tc-test-1value)))
	  (should (equal expected report)))))




(ert-deftest tc-cobertura-test-signature-1 ()
  "Test `testcover-cobertura--signature' on a few functions.
100% coverage is not acheived, and maye the rest of the function
is not needed?"
  :tags '(testcover-cobertura--signature)
  (should (string= "(testcover-cobertura--signature DEF)"
				   (testcover-cobertura--signature 'testcover-cobertura--signature)))
  (should (string= "(testcover-cobertura--collect-elem PATH FORM)"
				   (testcover-cobertura--signature 'testcover-cobertura--collect-elem))))



(ert-deftest tc-cobertura-test-line-rate-1 ()
  "Test "
  :tags '(testcover-cobertura--line-rate)
  (ert-info ("Test one line with no hits")
	(let ((fragment '((line :number 1 :hits 0 :branch "false"))))
	  (should (= 0 (testcover-cobertura--line-rate fragment)))))
  (ert-info ("Test one line with 1 hit")
	(let ((fragment '((line :number 1 :hits 1 :branch "false"))))
	  (should (= 1 (testcover-cobertura--line-rate fragment)))))
  (ert-info ("Test one line with 2 hits")
	(let ((fragment '((line :number 1 :hits 2 :branch "false"))))
	(should (= 1 (testcover-cobertura--line-rate fragment)))))
  (ert-info ("Test two lines with no hits")
	(let ((fragment '((line :number 1 :hits 0 :branch "false")
					  (line :number 2 :hits 0 :branch "false"))))
	  (should (= 0 (testcover-cobertura--line-rate fragment)))))
  (ert-info ("Test two lines with 2 hits each")
	(let ((fragment '((line :number 1 :hits 2 :branch "false")
					  (line :number 2 :hits 2 :branch "false"))))
	  (should (= 1 (testcover-cobertura--line-rate fragment)))))
  (ert-info ("Test two lines with 1 hit each")
	(let ((fragment '((line :number 1 :hits 1 :branch "false")
					  (line :number 2 :hits 1 :branch "false"))))
	(should (= 1 (testcover-cobertura--line-rate fragment)))))
  (ert-info ("Test two lines, one with one hit and one with no hits")
	(let ((fragment '((line :number 1 :hits 1 :branch "false")
					  (line :number 2 :hits 0 :branch "false"))))
	  (should (= 0.5 (testcover-cobertura--line-rate fragment)))))
  (ert-info ("Test three lines")
	(let ((fragment '((line :number 1 :hits 1 :branch "false")
					  (line :number 2 :hits 0 :branch "false")
					  (line :number 3 :hits 0 :branch "false"))))
	  (should (= (/ 1.0 3) (testcover-cobertura--line-rate fragment))))))

(ert-deftest tc-cobertura-test-line-rate-2 ()
  "Test nil argument."
  :tags '(testcover-cobertura--line-rate)
  (should (= 0.0 (testcover-cobertura--line-rate nil))))



(ert-deftest tc-cobertura-test-branch-rate-1 ()
  :tags '(testcover-cobertura--branch-rate)
  (ert-info ("No branch lines, branch rate 1.0")
	(let ((lines '((line :hits 1 :number 43 :branch "false" :condition-coverage 0))))
	  (should (= 1 (testcover-cobertura--branch-rate lines)))))
  (ert-info ("A single branch line, branch-rate should match the :condition-coverage")
	(let ((lines '((line :hits 1 :number 43 :branch "true" :condition-coverage 0.5))))
	  (should (= 0.5 (testcover-cobertura--branch-rate lines)))))
  (ert-info ("Five branch lines, branch-rate should match the mean of :condition-coverage")
	(let ((lines '((line :hits 1 :number 44 :branch "true" :condition-coverage 0.1)
				   (line :hits 1 :number 45 :branch "true" :condition-coverage 0.2)
				   (line :hits 1 :number 46 :branch "true" :condition-coverage 0.3)
				   (line :hits 1 :number 47 :branch "true" :condition-coverage 0.4)
				   (line :hits 1 :number 48 :branch "true" :condition-coverage 0.5))))
	  (should (= (/ (+ 0.1 0.2 0.3 0.4 0.5) 5) (testcover-cobertura--branch-rate lines)))))
  )

(ert-deftest tc-cobertura-test-branch-rate-2 ()
  :tags '(testcover-cobertura--branch-rate)
  (should (= 0.0 (testcover-cobertura--branch-rate nil))))



(ert-deftest tc-cobertura-test-file-report-1 ()
  "Test `testcover-cobertura-file-report' on a non-instrumented
file."
  :tags '(testcover-cobertura-file-report)
  (with-current-buffer (find-file-noselect tc-test1)
	(kill-local-variable 'edebug-form-data))
  (load-file tc-test1)
  (should (equal (testcover-cobertura-file-report tc-test1)
				 `(class :name ,(file-name-nondirectory tc-test1)
						 :filename ,tc-test1 :line-rate 0.0
						 :branch-rate 0.0 :complexity 0.0
						 (methods) (lines)))))

(ert-deftest tc-cobertura-test-file-report-2 ()
  "Test `testcover-cobertura-file-report' on an instrumented
but not run file."
  :tags '(testcover-cobertura-file-report)
  (with-current-buffer (find-file-noselect tc-test1)
	(kill-local-variable 'edebug-form-data))
  (tc-with-test-file tc-test1
	(let ((result (testcover-cobertura-file-report tc-test1))
		  (expect `(class :name "test1.el"
						  :filename ,tc-test1 :line-rate 0.0
						  :branch-rate 0.0 :complexity 0.0
						  (methods) (lines))))
	  (should (= (length result) (length expect)))
	  (dotimes (i (length expect))
		(let ((expected (nth i expect))
			  (res (nth i result)))
		  (cond
		   ((symbolp expected) (should (eq expected res)))
		   ((numberp expected) (should (= expected res)))
		   ((stringp expected) (should (string= expected res)))
		   ((consp expected) (should (eq (car expected) (car res))))
		   (t (ert-fail (format "Unexpected type in `expected': %s" expected))))
		  ))
	  (ert-info ("Check sub-elements")
		(let ((iter result)
			  methods lines)
		  (while iter
			(when (consp (car iter))
			  (cond ((eq (caar iter) 'methods)
					 (should (null methods))
					 (should (= (1+ 5) (length (car iter))))
					 (setq methods (cdar iter)))
					((eq (caar iter) 'lines)
					 (should (null lines))
					 (setq lines (cdar iter)))))
			(setq iter (cdr iter)))
		  (should (and methods lines))
		  (dolist (m methods)
			(let ((ml (find-if #'consp m)))
			  (should ml)
			  (should (eq 'lines (car ml)))
			  (dolist (l (cdr ml))
				(should (member l lines))
				(setq lines (remove l lines)))))
		  (should (null lines)))))))



(provide 'tc-cobertura-test)
;;; tc-cobertura-test.el ends here

;; Local Variables:
;; flycheck-emacs-lisp-load-path: ("..")
;; byte-compile-warnings: (not cl-functions)
;; End:
