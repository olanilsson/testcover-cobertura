;;; test1.el --- Meta tests for testcover-cobertura.el

;; Copyright (C) 2014  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;;; Commentary:

;; This file contains simple functions used to verify the
;; functionality of testcover-cobertura.el

;;; Code:

(defun tc-test-test-1 () "." 7)
(defun tc-test-test-2 (a) "A." (if a 1 2))
(defun tc-test-test-3 (a b c) "A B C." (if a (if b c a) (if c b a)))

(defun tc-test-1value (a) "A." (while (< 0 a) (setq a (1- a))))

(defun tc-test-lambda () "." (apply (lambda (a b) (+ a b)) 1 2))

(provide 'test1)
;;; test1.el ends here
