# testcover-cobertura - Cobertura XML from testcover data

[![Run Status](https://api.shippable.com/projects/540f5e6121c97efdb8989e99/badge?branch=master)](https://app.shippable.com/bitbucket/olanilsson/testcover-cobertura) 
[![Coverage Badge](https://api.shippable.com/projects/540f5e6121c97efdb8989e99/coverageBadge?branch=master)](https://app.shippable.com/bitbucket/olanilsson/testcover-cobertura) 
[![CircleCI](https://circleci.com/bb/olanilsson/testcover-cobertura.svg?style=shield&circle-token=4a14120de5fc9c8e6bfe0fd5988ab4807cdf501b)](https://circleci.com/bb/olanilsson/testcover-cobertura)

![emacs 24.3](https://img.shields.io/badge/emacs-24.3-brightgreen.svg) 
![emacs 24.4](https://img.shields.io/badge/emacs-24.4-brightgreen.svg)
![emacs 24.5](https://img.shields.io/badge/emacs-24.5-brightgreen.svg)
![emacs 25.1](https://img.shields.io/badge/emacs-25.1-brightgreen.svg) 
![emacs 25.2](https://img.shields.io/badge/emacs-25.2-brightgreen.svg) 
![emacs 25.3](https://img.shields.io/badge/emacs-25.3-brightgreen.svg) 

Generate [Cobertura](http://cobertura.github.io/cobertura/) code
coverage reports from data created using `testcover.el`.

Some [CI](http://en.wikipedia.org/wiki/Continuous_integration)
tools display results from Cobertura reports as part of
their testrun results.  The testcover-cobertura package was written
to support this feature on http://shippable.com.

`testcover` differs from other code coverage tools (like Cobertura)
in how line and branch hits are counted.  `testcover` tracks return
values of forms.  Each form can have three states;

* `unknown`:  not evaluated
* `value`: evaluated at least once but always returns the same value
* `testcover-ok`: evaluated more than once with at least two different values.

There are exceptions; certain forms are known to always return the same
value, etc.

So instead of using line hit count as Cobertura expects,
`testcover-cobertura` uses points.  An `testcover-ok` point is
assigned a hit count of 2, a `value` point a hit count of 1 and an
`unknown` is assigned hit count 0.  A function where all points
have a hit count of 2 is 100% covered.

Each form that has more than one possible value is considered a
branch, and that branch is fully covered once at least two values
have been returned.

This way of reporting `testcover` data gives the best result when
it comes to code coverage grade and branch coverage grade.  It is
unusable to highlight the original source code to show what code is
not covered.  It should not be hard to translate points to lines,
but the branches will probably prove more difficult.

## See also

[ert-junit](http://bitbucket.org/olanilsson/ert-junit) is a good
complement when running CI on http://shippable.com.

[Comparison of CI services](ci/ci.md)
