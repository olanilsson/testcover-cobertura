;;; testcover-cobertura.el --- Cobertura testcover reports

;; Copyright (C) 2014-2016  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Maintainer: Ola Nilsson <ola.nilsson@gmail.com>
;; Created; Aug 4 2014
;; Keywords: lisp tools test ert coverage
;; Version: 0.2
;; URL:  http://bitbucket.org/olanilsson/testcover-cobertura
;; Package-Requires: ((emacs "24.3") (xmlgen "0.4"))

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Generate Cobertura (http://cobertura.github.io/cobertura/) code
;; coverage reports from data created using `testcover.el'.
;;
;; Some CI tools display results from cobertura reports as part of
;; their testrun results.  The testcover-cobertura package was written
;; to support this feature on http://shippable.com.
;;
;; `testcover' differs from other code coverage tools (like Cobertura)
;; in how line and branch hits are counted.  `testcover' tracks return
;; values of forms.  Each form can have three states;
;; - `unknown':  not evaluated
;; - value: evaluated at least once but always returns the same value
;; - `testcover-ok': evaluated more than once with at least two different values.
;; There are exceptions; certain forms are known to always return the same
;; value, etc.
;;
;; So instead of using line hit count as Cobertura expects,
;; `testcover-cobertura' uses points.  An `testcover-ok' point is
;; assigned a hit count of 2, a `value' point a hit count of 1 and an
;; `unknown' is assigned hit count 0.  A function where all points
;; have a hit count of 2 is 100% covered.
;;
;; Each form that has more than one possible value is considered a
;; branch, and that branch is fully covered once at least two values
;; have been returned.
;;
;; This way of reporting `testcover' data gives the best result when
;; it comes to code coverage grade and branch coverage grade.  It is
;; unusable to highlight the original source code to show what code is
;; not covered.  It should not be hard to translate points to lines,
;; but the branches will probably prove more difficult.

;;; Code:

(require 'cl)
(require 'ert)
(require 'help-fns)
(require 'testcover)
(require 'xmlgen)

(defun testcover-cobertura--used-indexes (form)
  "Find the testcover indexes actually in use in FORM.
Return the used indexes in a sorted list.  When a function is
reinstrumented from edebug to testcover a vector of coverage
points is created.  Not all of the indexes in this vector are
used.  The ones that are not are marked as `ok-coverage', which
is indistinguishable from used points with `ok-coverage'.  By
recursively searching for instances of `testcover-after' in FORM
the indexes of actually used coverage points can be extracted.
This information can then be used to ignore the unused points
which otherwise inflate the coverage rate of FORM."
  (setq form (remove-if-not #'consp form))
  (let (points)
    (while form
      (let ((top (car form))
            (first (caar form))) ; car of top
        (setq form (cdr form))
        (cond ((eq 'quote first)) ; ignore quoted sexps
              ((and (symbolp first) ; keep points
                    (member first
                            '(testcover-after testcover-1value)))
               (destructuring-bind (tc-fun point inner) top
                 (setq points (cons point points))
                 (when (consp inner)
                   (setq form (cons inner form)))))
              (t (dolist (elt top)
                   (when (consp elt)
                     (setq form (cons elt form))))))))
    (sort points #'<)))

(defun testcover-cobertura-defun-report (def)
  "Return a genxml compatible Cobertura fragment for DEF.
The returned fragment is a xmlgen representation of the `method'
element of a Cobertura XML document."
  (let* ((data (get def 'edebug))
		 (coverage (get def 'edebug-coverage))
         def-mark points len used-points lines)
	(unless (consp data)
      (error "Missing edebug data for function %s" def))
    (setq def-mark (car data)
          points (nth 2 data)
          len (length points)
          used-points (testcover-cobertura--used-indexes (symbol-function def)))
	(while (> len 0)
	  (setq len (1- len)
			data (aref coverage len))
      (when (memq len used-points)
        (let* ((hits (cond ((eq data 'ok-coverage) 2) ;returned at least two values
                           ;; sexp marked 1value that has returned the
                           ;; same value each time evaled
                           ((eq (car-safe data) '1value) 2)
                           ;; sexp that has never been called
                           ((memq data '(unknown 1value)) 0)
                           (t 1)))
               (branch (not (eq (car-safe data) '1value)))
               (line (list 'line
                           :number (+ def-mark (aref points len))
                           :hits hits
                           :branch (if branch "true" "false"))))
          (when branch
            (setq line (append line (list :condition-coverage (/ hits 2.0))))
            ;;; It is possible to add a conditions element to the line
            ;;; element but so far I haven't understood it's purpose,
            ;;; and shippable works fine without it.
            ;; (when (> hits 0)
            ;;   (let ((conditions '(conditions)))
            ;;     (dotimes (n (min hits 2))
            ;;       (setq conditions
            ;;             (append conditions
            ;;                     (list (list 'condition :number n :type "jump" :coverage 0.5)))))
            ;;     (setq line (append line (list conditions)))))
            )
          (setq lines (cons line lines))))
      )
	(list 'method :name (symbol-name def)
          :signature (testcover-cobertura--signature def)
		  :line-rate (if lines (testcover-cobertura--line-rate lines) 1.0)
		  :branch-rate (if lines (testcover-cobertura--branch-rate lines) 1.0)
          (cons 'lines lines))))

(defmacro testcover-cobertura--help-make-usage (def arglist)
  "Call `help-make-usage' or `help--make-usage' based on Emacs major version.
DEF and ARGLIST are passed to the correct function.
`help-make-usage' changed name to `help--make-usage' in Emacs
25.1."
  (if (< emacs-major-version 25)
    `(help-make-usage ,def ,arglist)
  `(help--make-usage ,def ,arglist)))

(defun testcover-cobertura--signature (def)
  "Return a string containing the function signature of DEF."
  (let ((arglist (help-function-arglist def)))
    (format "%s"
            (testcover-cobertura--help-make-usage
             def
             (if (listp arglist)
                 arglist
               (let ((fn (symbol-function def)))
                 (case (car fn)
                   ('lambda (cadr fn))
                   ('defun (caddr fn)))))))))

(defun testcover-cobertura--collect-elem (path form)
  "Use PATH to extract elements from FORM.
The use is XPATH-ish:
  (testcover-cobertura--collect-elem '(a b c) '(a (b (c) (c) (c)) (b (d (c)))))
    => ((c) (c) (c))"
  ;; there must be a root element/tag/symbol that match between PATH
  ;; and FORM
  (when (and (listp form)
             (eq (car path) (car form)))
    (if (cdr path)
        (apply #'append
               (remove nil (mapcar (apply-partially #'testcover-cobertura--collect-elem (cdr path))
                                    (cdr form))))
    (list form))))

(defun testcover-cobertura--line-rate (lines)
  "Calculate the total line coverage of LINES.
LINES shuould be a list of lists where each inner list contains
an element `:hits' followed by an integer indicating how many
times that line has been hit.  The line rate is calculated as the
number of lines with a hit count of at least one divided by the
total number of lines."
  (if (null lines) 0.0
    (/ (float (count 1 lines :test #'<=
                     :key (lambda (line) (cadr (member :hits line)))))
       (length lines))))

(defun testcover-cobertura--branch-rate (lines)
  "Calculate the branch rate of LINES.
LINES should be a list of lists where each inner list contains an
embedded plist with the keys :branch and :condition-coverage .
The returned branch rate is the mean value of
the :condition-coverage values for all elements (lines) where
the :branch value is \"true\"."
  (if (null lines)
      0.0
    ;; first remove all lines that doesn't have a :branch attribute or
    ;; where the value of that attribute is not true.
    (let ((branch-lines
           (remove* "true" lines
                    :test-not (lambda (str line)
                                (string= (cadr (member :branch line))
                                         (1value str))))))
      (if (not branch-lines) 1.0 ;; branches means full coverage
          (/ (apply #'+ (mapcar (lambda (bline)
                                (cadr (member :condition-coverage bline)))
                                branch-lines))
             (max (length branch-lines) 1)))
      )))

(defun testcover-cobertura-file-report (filename)
  "Return a `xmlgen' compatible cobertura coverage report for FILENAME."
  (let ((buf (find-file-noselect filename))
		methods lines)
	(with-current-buffer buf
	  (dolist (x edebug-form-data)
        ;; FIXME: include anon functions somehow
        (unless (string-match "edebug-anon[0-9]+" (symbol-name (car x)))
		(setq methods
			  (cons
               (testcover-cobertura-defun-report (car x))
			   methods)))))
	(setq lines
          (apply #'append
                 (mapcar (lambda (method)
                           (copy-list (cdr (some (lambda (e)
                                                   (and (listp e) e))
                                                 method))))
                         methods)))
	(list 'class :name (file-name-nondirectory filename) :filename filename
          :line-rate (testcover-cobertura--line-rate lines)
		  :branch-rate (testcover-cobertura--branch-rate lines)
          :complexity 0.0
          (cons 'methods methods)
		  (cons 'lines lines))))

(defun testcover-cobertura-package-report (pkgname elfiles)
  "Generate a `package' xml-fragment for a cobertura report.
PKGNAME should be a string used a the package name.  ELFILES
should be a list of files to be included in the package."
  (let* ((classes (mapcar #'testcover-cobertura-file-report elfiles))
         (lines (testcover-cobertura--collect-elem '(classes class lines line)
                                                   (cons 'classes classes))))
    (list 'package :name pkgname
          :line-rate (testcover-cobertura--line-rate lines)
          :branch-rate (testcover-cobertura--branch-rate lines)
          :complexity 0.0
          (cons 'classes classes))))

(defun testcover-cobertura--break-and-indent-xml (buffer)
   "Line break and indent the XML code in BUFFER.
Makes it easier to view the file in Emacs."
  (with-current-buffer buffer
    (save-excursion
      (goto-char (point-min))
      (forward-sexp)
      (while (char-after)
        (when (and (= (char-after) ?<)
                   (= (char-before) ?>))
          (newline-and-indent))
        (forward-sexp))
      (indent-region (point-min) (point-max))
      (save-buffer))))

(defun ert-with-coverage (result-file elfiles &optional selector byte-compile pretty)
  "Run all `ert' test(s) with `testcover' and generate cobertura report.
Return the result of `ert-run-tests-batch'.  RESULT-FILE is the
name of the cobertura xml file.  ELFILES is a list of the files
that shall be instrumented before running the tests.  SELECTOR is
passed to `ert-run-tests-batch', and BYTE-COMPILE is passed to
`testcover-start'.  If PRETTY is non-nil, break and indent the
xml."
  (dolist (elfile elfiles)
    (testcover-start elfile byte-compile))
  (let* ((stats (ert-run-tests-batch selector))
         ;; there is only ever a single package from testcover-cobertura-package-report
         (packages (list (testcover-cobertura-package-report "" elfiles)))
         (lines (testcover-cobertura--collect-elem '(packages package classes class lines line)
                              (cons 'packages packages)))
         (outbuf (find-file-noselect result-file)))
    (with-current-buffer outbuf
      (buffer-disable-undo)
      (widen)
      (delete-region (point-min) (point-max))
      (insert
       "<?xml version=\"1.0\" ?>\n"
       "<!DOCTYPE coverage\n"
       "          SYSTEM 'http://cobertura.sourceforge.net/xml/coverage-04.dtd'>\n"
       "<!-- Generated by testcover-cobertura.el: https://bitbucket.org/olanilsson/testcover-cobertura -->\n"
       (xmlgen
               (list 'coverage
                     :line-rate (testcover-cobertura--line-rate lines)
                     :branch-rate (testcover-cobertura--branch-rate lines)
                     ;;; more attributes to add at a later date.
                     ;; :lines-covered (count 0 lines
                     ;;                       :key (lambda (e) (plist-get (cdr e) :hits)))
                     ;; :lines-valid (length lines)
                     ;; :branches-covered 0
                     ;; :branches-valid 0
                     ;; :complexity 0.0
                     :version "3.7.1"
                     :timestamp (replace-regexp-in-string
                                 "\\..*" ""
                                 (number-to-string (float-time)))
                     ;; <sources><source>file1</source><source>file2</source>...</sources>
                     (cons 'sources (mapcar (lambda (efile) (list 'source efile)) elfiles))
                     (cons 'packages packages))))
      (save-buffer)
      (when pretty
        (testcover-cobertura--break-and-indent-xml (current-buffer))))
    stats))

(defun ert-run-cobertura-tests-batch-and-exit (&optional selector byte-compile pretty)
  "Run all `ert' test(s) with `testcover' and generate cobertura report.
First command line argument should be the name of the report
file, the rest of the command line arguments elisp files to
instrument before running the tests.  SELECTOR is passed to
`ert-run-tests-batch'.  BYTE-COMPILE is passed to
`testcover-start' when the elisp files are instrumented.  PRETTY
is passed to `ert-with-coverage' on-nil, to indicate the xml
should be formatted human readable."
  (unwind-protect
      (let* ((result-file (and command-line-args-left
                               (>= (length command-line-args-left) 1)
                               (car command-line-args-left)))
             (elfiles (remove* ".*\\.el" (cdr command-line-args-left) :test-not #'string-match))
             (stats (ert-with-coverage result-file elfiles selector byte-compile pretty)))
          (kill-emacs (if (zerop (ert-stats-completed-unexpected stats)) 0 1)))
    (backtrace))
  (unwind-protect
      (progn
        (message "Error running tests")
        (backtrace))
    (kill-emacs 2)))

(provide 'testcover-cobertura)
;;; testcover-cobertura.el ends here

;; Local Variables:
;; byte-compile-warnings: (not cl-functions)
;; indent-tabs-mode: nil
;; tab-width: 4
;; End:
