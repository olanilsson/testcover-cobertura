# Continous integration evaluation

I've used this project to evaluate some different
[CI](http://en.wikipedia.org/wiki/Continuous_integration) services.

## Shippable [http://shippable.com][SHIPPABLE]

[Shippable][SHIPPABLE] was early with supporting
[Bitbucket](BB) and the first CI service I tested.

Configuration is done with a
[shippable.yml](http://docs.shippable.com/ci/shippableyml/#anatomy-of-shippableyml)
file in your repository.  Some additional services - such as
[pipelines](http://docs.shippable.com/pipelines/overview/) - requires
additional [yml](https://en.wikipedia.org/wiki/Yaml) files.

It supports parsing of [JUnit][JUNIT] test reports and
[Cobertura][COBERTURA] coverage reports.  The test display only show
number of tests, for a detailed report you have to download the xml
file.  The coverage display shows branch and sequence coverage per
file, but there is no source code display.

Shippable let you specify a N-dimensional test matrix where each
dimension is associated with an environment variable and a list of
values.  Each item in the matrix has its own test and coverage report.
Tests in the matrix can run in parallel if you have more than one
minion, but the free plan includes only a single minion.

Unfortunately there is no simple way to run scrips on the condition
that all sub-test runs pass.

Shippable have a deployment concept called pipelines, but you only get
one in the free plan.  I'm still unclear on how many repos can use the
single pipeline, as I've not yet managed to set it up.

All tests run in [Docker](http://www.docker.com) containers, and the
entire Shippable service is very Docker centric.  Configuring your own
Docker images can speed up testing significantly.

## CircleCi [http://cirleci.com][CIRCLE]

Configuration is done with a
[circle.yml](https://circleci.com/docs/configuration/) file in your
repository.

Displays summaries of test results from [JUnit][JUNIT] xml report
files, but for detailed information you have to download the files.
The results of several files are automatically aggregated.

Several types of coverage reports are supported, but not Cobertura.

CircleCi supports
[parallelism](https://circleci.com/docs/parallelism/).  The free plan
only includes one parallel container, open source projects get four.
Additional containers are available in the paid plans.

There is no test matrix, and parallelism has to be
[manually configured](https://circleci.com/docs/parallel-manual-setup/#balancing)
unless you use one of the supported frameworks.  For supported
frameworks the test load will be spread across the containers.

## semaphore [https://semaphoreci.com][SEM]

[Semaphore][SEM] is configured on the semaphore ci webpage in dialog boxes.

[BB]: http://bitbucket.org
[CIRCLE]: http://cirleci.com
[COBERTURA]: http://cobertura.github.io/cobertura/
[JUNIT]: http://junit.org
[SEM]: https://semaphoreci.com
[SHIPPABLE]: http://shippable.com
