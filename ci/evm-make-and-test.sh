#!/bin/sh
#echo \*\*\*\* Run tests on Emacs $1
#set -e
evm install --use emacs-$1-travis
ci/make-and-test.sh $2
#make lisp-clean
#make
#make test JUNIT_OUT=$2
