#!/bin/sh
echo \*\*\*\* Run tests on Emacs $EMACS
set -e
make lisp-clean
make
make test JUNIT_OUT=$1
