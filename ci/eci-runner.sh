#!/bin/bash

errorcounter() {
	(( errcount++ ))
}

trap errorcounter ERR

: ${EVERS:="24.3 24.4 24.5 25.1 25.2 25.3"}
for ever in $EVERS; do
	export EMACS=/opt/emacs/$ever/bin/emacs
	ci/make-and-test.sh $CIRCLE_TEST_REPORTS/$ever/junit.xml
done

exit ${errcount:-0}
