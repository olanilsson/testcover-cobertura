#!/bin/bash
#
total=$2
index=$1

errorcounter() {
	(( errcount++ ))
}

trap errorcounter ERR

: ${EVMS:="24.3 24.4 24.5 25.1 25.2 git-snapshot"}
evms=$(echo $EVMS | tr ' ' '\n' | awk "NR % $total == $index")
for ever in $evms; do
	ci/evm-make-and-test.sh $ever $CIRCLE_TEST_REPORTS/$ever/junit.xml
done

exit ${errcount:-0}
